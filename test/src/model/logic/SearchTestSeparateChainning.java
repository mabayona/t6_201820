package model.logic;
import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

import junit.framework.TestCase;
import model.data_structures.SeparateChainingHashTable;
public class SearchTestSeparateChainning extends TestCase{

	

	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------
	
	private SeparateChainingHashTable<Integer, Integer> hashTable03;
	private SeparateChainingHashTable<Integer, Integer> hashTable05;
	private SeparateChainingHashTable<Integer, Integer> hashTable07;
	private Random random;;

	

	//--------------------------------------------------------
	//Escenarios
	//--------------------------------------------------------
	
	public void setUpEscenario0() {
		hashTable03 = new SeparateChainingHashTable<Integer, Integer>(1000);
		hashTable03.setLoadFactor(3);
		random = new Random();
	}  

	public void setUpEscenario1() {
		hashTable05 = new SeparateChainingHashTable<Integer, Integer>(1000);
		hashTable05.setLoadFactor(5);
		random = new Random();
	}  
	public void setUpEscenario2() {
		hashTable07 = new SeparateChainingHashTable<Integer, Integer>(1000);
		hashTable07.setLoadFactor(7);
		random = new Random();
	}  

	//--------------------------------------------------------
	//Metodos
	//--------------------------------------------------------

	public void testConFactorDeCargaDe07() {
		setUpEscenario2();
		System.out.println("...cargando");
		//TIEMPO CARGA
		//===========================================
		long startTimeCarga = System.currentTimeMillis();
		for(int i = 0; i < 1000000; i++) {
			hashTable07.put(i, random.nextInt(100));
		}
		long endTimeCarga = System.currentTimeMillis();
		
		long duracionCarga = endTimeCarga - startTimeCarga; 
		//===========================================
		
		//TIEMPO CONSULTA
		//===========================================
		long tiemposConsulta = 0;

		for(int i = 1; i <= 10000; i++) {
			long startTimeConsulta = System.currentTimeMillis();
			hashTable07.get(random.nextInt(900000));
			long endTimeConsulta = System.currentTimeMillis();
			
			long duracionConsulta = endTimeConsulta - startTimeConsulta; 
			tiemposConsulta += duracionConsulta;
		}
		
		double promedio = tiemposConsulta/10000;
		//===========================================

		System.out.println("==================== LinearProbingHashTable ===========================\n");
		System.out.println("FACTOR DE CARGA: 7 \n");
		System.out.println("TIEMPO DE CARGA (1 millon de elementos): "+ duracionCarga +" milisegundos\n");
		System.out.println("TIEMPO PROMEDIO DE CONSULTA (10 mil consultas): "+ promedio +" milisegundos\n");
		System.out.println("=======================================================================\n");
		System.out.println("\n");
		
	}
	public void testConFactorDeCargaDe05() {
		setUpEscenario1();
		System.out.println("...cargando");
		//TIEMPO CARGA
		//===========================================
		long startTimeCarga = System.currentTimeMillis();
		for(int i = 0; i < 1000000; i++) {
			hashTable05.put(i, random.nextInt(100));
		}
		long endTimeCarga = System.currentTimeMillis();
		
		long duracionCarga = endTimeCarga - startTimeCarga; 
		//===========================================
		
		//TIEMPO CONSULTA
		//===========================================
		long tiemposConsulta = 0;

		for(int i = 1; i <= 10000; i++) {
			long startTimeConsulta = System.currentTimeMillis();
			hashTable05.get(random.nextInt(900000));
			long endTimeConsulta = System.currentTimeMillis();
			
			long duracionConsulta = endTimeConsulta - startTimeConsulta; 
			tiemposConsulta += duracionConsulta;
		}
		
		double promedio = tiemposConsulta/10000;
		//===========================================

		System.out.println("==================== LinearProbingHashTable ===========================\n");
		System.out.println("FACTOR DE CARGA: 5 \n");
		System.out.println("TIEMPO DE CARGA (1 millon de elementos): "+ duracionCarga +" milisegundos\n");
		System.out.println("TIEMPO PROMEDIO DE CONSULTA (10 mil consultas): "+ promedio +" milisegundos\n");
		System.out.println("=======================================================================\n");
		System.out.println("\n");
	}
	
	
	
	public void testConFactorDeCargaDe03() {
		setUpEscenario0();
		System.out.println("...cargando");
		//TIEMPO CARGA
		//===========================================
		long startTimeCarga = System.currentTimeMillis();
		for(int i = 0; i < 1000000; i++) {
			hashTable03.put(i, random.nextInt(100));
		}
		long endTimeCarga = System.currentTimeMillis();
		
		long duracionCarga = endTimeCarga - startTimeCarga; 
		//===========================================
		
		//TIEMPO CONSULTA
		//===========================================
		long tiemposConsulta = 0;

		for(int i = 1; i <= 10000; i++) {
			long startTimeConsulta = System.currentTimeMillis();
			hashTable03.get(random.nextInt(900000));
			long endTimeConsulta = System.currentTimeMillis();
			
			long duracionConsulta = endTimeConsulta - startTimeConsulta; 
			tiemposConsulta += duracionConsulta;
		}
		
		double promedio = tiemposConsulta/10000;
		//===========================================

		System.out.println("==================== LinearProbingHashTable ===========================\n");
		System.out.println("FACTOR DE CARGA: 3 \n");
		System.out.println("TIEMPO DE CARGA (1 millon de elementos): "+ duracionCarga +" milisegundos\n");
		System.out.println("TIEMPO PROMEDIO DE CONSULTA (10 mil consultas): "+ promedio +" milisegundos\n");
		System.out.println("=======================================================================\n");
		System.out.println("\n");
	}

}
