package model.logic;

import java.util.Iterator;
import java.util.Random;

import junit.framework.TestCase;
import model.data_structures.LinearProbingHashTable;

public class SearchTestLinearProbing extends TestCase{

	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------
	
	private LinearProbingHashTable<Integer, Integer> hashTable025;
	private LinearProbingHashTable<Integer, Integer> hashTable050;
	private LinearProbingHashTable<Integer, Integer> hashTable075;
	private Random random;;

	

	//--------------------------------------------------------
	//Escenarios
	//--------------------------------------------------------
	
	public void setUpEscenario0() {
		hashTable025 = new LinearProbingHashTable<Integer, Integer>(1000);
		hashTable025.setLoadFactor(0.25);
		random = new Random();
	}  

	public void setUpEscenario1() {
		hashTable050 = new LinearProbingHashTable<Integer, Integer>(1000);
		hashTable050.setLoadFactor(0.50);
		random = new Random();
	}  
	public void setUpEscenario2() {
		hashTable075 = new LinearProbingHashTable<Integer, Integer>(1000);
		hashTable075.setLoadFactor(0.75);
		random = new Random();
	}  

	//--------------------------------------------------------
	//Metodos
	//--------------------------------------------------------

	public void testConFactorDeCargaDe075() {
		setUpEscenario2();
		System.out.println("...cargando");
		//TIEMPO CARGA
		//===========================================
		long startTimeCarga = System.currentTimeMillis();
		for(int i = 0; i < 1000000; i++) {
			hashTable075.put(i, random.nextInt(100));
		}
		long endTimeCarga = System.currentTimeMillis();
		
		long duracionCarga = endTimeCarga - startTimeCarga; 
		//===========================================
		
		//TIEMPO CONSULTA
		//===========================================
		long tiemposConsulta = 0;

		for(int i = 1; i <= 10000; i++) {
			long startTimeConsulta = System.currentTimeMillis();
			hashTable075.get(random.nextInt(900000));
			long endTimeConsulta = System.currentTimeMillis();
			
			long duracionConsulta = endTimeConsulta - startTimeConsulta; 
			tiemposConsulta += duracionConsulta;
		}
		
		double promedio = tiemposConsulta/10000;
		//===========================================

		System.out.println("==================== LinearProbingHashTable ===========================\n");
		System.out.println("FACTOR DE CARGA: 0.75 \n");
		System.out.println("TIEMPO DE CARGA (1 millon de elementos): "+ duracionCarga +" milisegundos\n");
		System.out.println("TIEMPO PROMEDIO DE CONSULTA (10 mil consultas): "+ promedio +" milisegundos\n");
		System.out.println("=======================================================================\n");
		System.out.println("\n");
		
	}
	public void testConFactorDeCargaDe050() {
		setUpEscenario1();
		System.out.println("...cargando");
		//TIEMPO CARGA
		//===========================================
		long startTimeCarga = System.currentTimeMillis();
		for(int i = 0; i < 1000000; i++) {
			hashTable050.put(i, random.nextInt(100));
		}
		long endTimeCarga = System.currentTimeMillis();
		
		long duracionCarga = endTimeCarga - startTimeCarga; 
		//===========================================
		
		//TIEMPO CONSULTA
		//===========================================
		long tiemposConsulta = 0;

		for(int i = 1; i <= 10000; i++) {
			long startTimeConsulta = System.currentTimeMillis();
			hashTable050.get(random.nextInt(900000));
			long endTimeConsulta = System.currentTimeMillis();
			
			long duracionConsulta = endTimeConsulta - startTimeConsulta; 
			tiemposConsulta += duracionConsulta;
		}
		
		double promedio = tiemposConsulta/10000;
		//===========================================

		System.out.println("==================== LinearProbingHashTable ===========================\n");
		System.out.println("FACTOR DE CARGA: 0.50 \n");
		System.out.println("TIEMPO DE CARGA (1 millon de elementos): "+ duracionCarga +" milisegundos\n");
		System.out.println("TIEMPO PROMEDIO DE CONSULTA (10 mil consultas): "+ promedio +" milisegundos\n");
		System.out.println("=======================================================================\n");
		System.out.println("\n");
	}
	
	
	
	public void testConFactorDeCargaDe025() {
		setUpEscenario0();
		System.out.println("...cargando");
		//TIEMPO CARGA
		//===========================================
		long startTimeCarga = System.currentTimeMillis();
		for(int i = 0; i < 1000000; i++) {
			hashTable025.put(i, random.nextInt(100));
		}
		long endTimeCarga = System.currentTimeMillis();
		
		long duracionCarga = endTimeCarga - startTimeCarga; 
		//===========================================
		
		//TIEMPO CONSULTA
		//===========================================
		long tiemposConsulta = 0;

		for(int i = 1; i <= 10000; i++) {
			long startTimeConsulta = System.currentTimeMillis();
			hashTable025.get(random.nextInt(900000));
			long endTimeConsulta = System.currentTimeMillis();
			
			long duracionConsulta = endTimeConsulta - startTimeConsulta; 
			tiemposConsulta += duracionConsulta;
		}
		
		double promedio = tiemposConsulta/10000;
		//===========================================

		System.out.println("==================== LinearProbingHashTable ===========================\n");
		System.out.println("FACTOR DE CARGA: 0.25 \n");
		System.out.println("TIEMPO DE CARGA (1 millon de elementos): "+ duracionCarga +" milisegundos\n");
		System.out.println("TIEMPO PROMEDIO DE CONSULTA (10 mil consultas): "+ promedio +" milisegundos\n");
		System.out.println("=======================================================================\n");
		System.out.println("\n");
	}

}
