package controller;

import javax.sound.midi.VoiceStatus;

import api.IDivvyTripsManager;
import model.data_structures.DoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOBikeRoute;
import model.vo.VOSector;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static DivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadBikeRoutesJSON() {
		manager.loadBikeRoutesJSON("./data/CDOT_Bike_Routes_2014_1216-transformed.json");
		manager.cargarBicisASectoresChainHash();
		manager.cargarBicisASectoresLinearPro();
	}
	
	public static DoublyLinkedList<VOBikeRoute> getBikeRoutes() {
		return manager.getRoutes();
	}
	
	public static String latitudesLongitudesMaxMin() {
		return manager.longsYLatsMayMin();
	}
	public static DoublyLinkedList<VOBikeRoute> bicicletasEnUnSector(double longitudP, double latitudP){
		return manager.bicicletasEnUnSector(longitudP, latitudP);
	}
	public static VOSector SectorDelPunto(double longitudP, double latitudP){
		return manager.sectorDelPunto(longitudP, latitudP);
	}
	
	public static VOSector SectorDelPuntoLinearPro(double longitudP, double latitudP){
		return manager.sectorDelPuntoLinearPro(longitudP, latitudP);
	}

}
