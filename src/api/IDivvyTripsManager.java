package api;

import model.data_structures.DoublyLinkedList;
import model.vo.VOBikeRoute;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager {

	public void loadBikeRoutesJSON(String jsonRoute) ;
	
	
	
}
