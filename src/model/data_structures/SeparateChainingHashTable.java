package model.data_structures;

public class SeparateChainingHashTable <Key, Value> {

	private static final int INIT_CAPACITY = 4;

	private int cuplas;                                // numero de cuplas key-value
	private int arregloTam; 
	private int loadFactor;// tamano del hash table
	private SequentialSearchST<Key, Value>[] arreglo;  // arreglo de linnked list symbol tables


	/**
	 * Initializa una tabla vacia con capacidad de 4
	 */
	public SeparateChainingHashTable() {
		this(INIT_CAPACITY);
	} 

	/**
	 * Initializa una tabla vacia de m listas encadenadas
	 * @param m numero inicial de lisatas encadenadas
	 */
	public SeparateChainingHashTable(int m) {
		this.arregloTam = m;
		loadFactor=5;
		arreglo = (SequentialSearchST<Key, Value>[]) new SequentialSearchST[m];
		for (int i = 0; i < m; i++)
			arreglo[i] = new SequentialSearchST<Key, Value>();
	} 

	public void setLoadFactor(int loadFactor) {
		this.loadFactor = loadFactor;
	}

	/**
	 * Reconfigura el tamano del arreglo para tener un numero dado de espacios
	 * rehashes all of the keys
	 */ 
	private void resize(int chains) {
		SeparateChainingHashTable <Key, Value> temp = new SeparateChainingHashTable<Key, Value>(chains);
		for (int i = 0; i < arregloTam; i++) {
			for (Key key : arreglo[i].keys()) {
				temp.put(key, arreglo[i].get(key));
			}
		}
		this.arregloTam  = temp.arregloTam;
		this.cuplas  = temp.cuplas;
		this.arreglo = temp.arreglo;
	}


	/**
	 *  hash value between 0 and arregloTam-1
	 */
	private int hash(Key key) {
		return (key.hashCode() & 0x7fffffff) % arregloTam;
	} 

	/**
	 * Retorna el numero de cuplas key-value en la tabla entera.
	 * @return el numero de cuplas key-value en la tabla entera.
	 */
	public int size() {
		return cuplas;
	} 

	/**
	 * Retorna true si la tabla no tiene elementos (cuplas key-value)
	 * @return {@code true} si la tabla no tiene elementos;
	 *         {@code false} de lo contrario
	 */
	public boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * Retorna true si la tabla contiene una llave especifica.
	 * 
	 * @param  key el key
	 * @return {@code true} si la tabla contiene el key dado por parametro;
	 *         {@code false} de lo contrario
	 * @throws IllegalArgumentException si el key == null
	 */
	public boolean contains(Key key) {
		if (key == null) throw new IllegalArgumentException("argumento de contains() es null");
		return get(key) != null;
	} 

	/**
	 * Retorna el valos asociado a la una llave en el hash table.
	 *
	 * @param  key el key
	 * @return el valor asociado a el key en el hash table;
	 *         null si no existe el valor
	 * @throws IllegalArgumentException si el key es null
	 */
	public Value get(Key key) {
		if (key == null) throw new IllegalArgumentException("argumento de get() es null");
		int i = hash(key);
		return arreglo[i].get(key);
	} 

	/**
	 * Inserta el key y el valor en la tabla, los valores viejos se pierden 
	 * 
	 * Se cambia el valor viejo por el nuevo si el key existe en la tabla.
	 * 
	 * Elimina el key y el value si el value de parametro==null
	 *
	 * @param  key el key
	 * @param  val el value
	 * @throws IllegalArgumentException si key==null
	 */
	public void put(Key key, Value val) {
		if (key == null) throw new IllegalArgumentException("primer argumento de put() es null");
		if (val == null) {
			delete(key);
			return;
		}

		// double table size if average length of list >= 10
		if ((cuplas / arregloTam)>loadFactor) resize(2*arregloTam);

		int i = hash(key);
		if (!arreglo[i].contains(key)) cuplas++;
		arreglo[i].put(key, val);
	} 

	/**
	 * Elimina el key y su valor asociado de la tabla hash     
	 * si el key esta en el hash table    
	 *
	 * @param  key el key
	 * @throws IllegalArgumentException si key==null
	 */
	public Value delete(Key key) {
		if (key == null) throw new IllegalArgumentException("argument to delete() is null");

		Value respuesta=get(key);
		int i = hash(key);
		if (arreglo[i].contains(key)) cuplas--;
		arreglo[i].delete(key);

		// parte a la mitad la tabla si el promedio por lista <= 2
		if (arregloTam > INIT_CAPACITY && cuplas <= 2*arregloTam) resize(arregloTam/2);
		return respuesta;
		
		
	}

	// retorna keys en la tabla como un Iterable
	public Iterable<Key> keys() {
		Queue<Key> queue = new Queue<Key>();
		for (int i = 0; i < arregloTam; i++) {
			if(arreglo[i] != null) {
				for (Key key : arreglo[i].keys())
					queue.enqueue(key);
			}
		}
		return queue;
	} 
	
	public int sizeKeys() {
		Queue<Key> queue = new Queue<Key>();
		for (int i = 0; i < arregloTam; i++) {
			
			for (Key key : arreglo[i].keys())
				if(key!=null) {
				queue.enqueue(key);
				}
		}
		return queue.size();
	}
	
	public int getArregloTam() {
		return arregloTam;
	}
	

}
