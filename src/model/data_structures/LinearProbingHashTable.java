package model.data_structures;

import java.util.Iterator;

public class LinearProbingHashTable<K, V> {

	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------

	private int keysNumber;

	private int size;

	private K[] keys;

	private V[] values;
	
	private double loadFactor;

	//--------------------------------------------------------
	//Constructores
	//--------------------------------------------------------

	@SuppressWarnings("unchecked")
	public LinearProbingHashTable(int size) {
		this.size = size;
		keys = (K[]) new Object[size];
		values = (V[]) new Object[size]; 
		loadFactor = 0.75;
	}

	//--------------------------------------------------------
	//Metodos publicos
	//--------------------------------------------------------

	/**
	 * Inserta el valor y la llave que se pasan por paramentro en el hashTable. 
	 * @param key Llave asociada al objeto. key != null
	 * @param value Valor que se quiere insertar. value!= null
	 */
	public void put(K key, V value) {
		if ((keysNumber/ size) >= loadFactor) rehash(2*size);     
		int i;      
		for (i = hash(key); keys[i] != null; i = (i + 1) % size) {
			if (keys[i].equals(key)) { 
				values[i] = value; 
				return; 
			}      
		}
		keys[i] = key;      
		values[i] = value;       
		keysNumber++; 

	}

	/**
	 * Determina si una llave existe en el arreglo keys
	 * @param key. La llave que se desea buscar. key!=null
	 * @return true, si la llave existe, false de lo contrario
	 */
	public boolean containsKey(K key) {
		for(K keyTemp : keys) {
			if(keysNumber == 0)
				return false;
			else if(keyTemp != null)	
				if(keyTemp.equals(key))
					return true;
		}
	
		return false;
	}

	/**
	 * Retorna el valor asociado a la llave o null si la llave no  tiene ningun valor asociado.  
	 * @param key La llave asociada al valor, key != null
	 * @return el valor buscado, o null si no existe
	 */
	public V get(K key) {
		V valor = null;
		for (int i = hash(key); keys[i] != null; i = (i + 1) % size) {
			if (keys[i].equals(key)) {
				valor = (V)values[i];
			}
		}
		return valor;
	}

	/**
	 * Elimina un valor y su llave asociada del HashTable. Retorna el valor elimiando
	 * @param key La llave asociada al valor que se desea eliminar, key != null
	 * @return el valor eliminado, o null si no existe
	 */
	public V delete(K key) {
		V deleted = null;
		if (!containsKey(key)) 
			return deleted;   
		int i = hash(key);   
		while (!key.equals(keys[i])) {
			i = (i + 1) % size;   
		}
		deleted = values[i];
		keys[i] = null;   
		values[i] = null;   
		i = (i + 1) % size;   
		while (keys[i] != null)   {      
			K  keyToRedo = keys[i];      
			V valToRedo = values[i];      
			keys[i] = null;      
			values[i] = null;     
			keysNumber--;        
			put(keyToRedo, valToRedo);      
			i = (i + 1) % size;   
		}   
		keysNumber--;      
		if (keysNumber > 0 && keysNumber == size/8) 
			rehash(size/2); 
		
		return deleted;
	}

	public Iterator<K> keysIterator(){
		DoublyLinkedList<K> list = new DoublyLinkedList<K>();
		for(K key : keys) {
			if(key != null)
			list.addAtEnd(key);
		}
		return list.iterator();
	}
	
	public int sizeKeys() {
		return keysNumber;
	}
	
	public int maxSize() {
		return size;
	}
	
	public double getLoaFactor() {
		return loadFactor;
	}
	
	public void setLoadFactor(double loadF) {
		this.loadFactor = loadF;
	}

	//--------------------------------------------------------
	//Metodos privados/auxiliares
	//--------------------------------------------------------

	private int hash(K key) {  
		return (key.hashCode() & 0x7fffffff) % size; 
	}

	private void rehash(int newSize) {
		 LinearProbingHashTable<K, V> newHT = new LinearProbingHashTable<K, V>(newSize);    
		 for (int i = 0; i < size; i++) {
			 if (keys[i] != null)           
				 newHT.put(keys[i], values[i]);    
		 }
		 keys = newHT.keys;    
		 values = newHT.values;    
		 size = newHT.size;
	}


}
