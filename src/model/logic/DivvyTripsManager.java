package model.logic;

import api.IDivvyTripsManager;
import model.vo.GeoCoordinate;
import model.vo.VOBikeRoute;
import model.vo.VOSector;
import model.data_structures.DoublyLinkedList;
import model.data_structures.LinearProbingHashTable;
import model.data_structures.SeparateChainingHashTable;
//test234
public class DivvyTripsManager implements IDivvyTripsManager {
	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------
	/*
	 * Lista de rutas
	 */
	private DoublyLinkedList<VOBikeRoute> routes;

	/*
	 * Administrador de Datos
	 */
	private DataManager dataManager;
	/*
	 * hash chaining table de sectores;
	 */
	private SeparateChainingHashTable <Integer, VOSector> sectoresChain;

	private LinearProbingHashTable<Integer, VOSector> sectoresProbing;

	private VOSector cuadranteCiudad;

	//--------------------------------------------------------
	//Constructor
	//--------------------------------------------------------

	public DivvyTripsManager() {
		dataManager = new DataManager();
		routes = new DoublyLinkedList<VOBikeRoute>();

	}
	//--------------------------------------------------------
	//Metodos
	//--------------------------------------------------------
	public void loadBikeRoutesJSON(String jsonRoute) {
		routes= dataManager.loadBikeRoutesJSON(jsonRoute);
		cuadranteCiudad = darCuadranteLimiteCiudad();
		crearHashSectores();

	}

	public VOSector darCuadranteLimiteCiudad() {

		VOSector limites = new VOSector();

		limites.setMaxLatitud(this.mayorLatitudRutas());
		limites.setMaxLongitud(this.mayorLongitudRutas());
		limites.setMinLatitud(this.menorLatitudRutas());
		limites.setMinLongitud(this.menorLongitudRutas());
		return limites;

	}

	public String longsYLatsMayMin() {
		return "Longitud m�xima: "+ mayorLongitudRutas() +", longitud m�nima: "+ menorLongitudRutas() +", latitud m�xima: "+ mayorLatitudRutas() +" y latitud m�nima: "+menorLatitudRutas();
	}

	/**
	 * Keys=id, Value=VOSector
	 */
	public void crearHashSectores() {
		sectoresChain=new SeparateChainingHashTable<Integer, VOSector>(100);
		sectoresProbing=new LinearProbingHashTable<Integer, VOSector>(100);
		
		double longMaxGlobal=cuadranteCiudad.getMaxLongitud();
		double longMinGlobal=cuadranteCiudad.getMinLongitud();
		double diferenciaLong = longMaxGlobal-longMinGlobal;
		
		double latMaxGlobal=cuadranteCiudad.getMaxLatitud();
		double latMinGlobal=cuadranteCiudad.getMinLatitud();
		double diferenciaLat=latMaxGlobal-latMinGlobal;
		
		int idCuenta=0;
		
		for (int i = 0; i < 9; i++) {

			//el maximo en latitud del sector, limite superior
			double latFinalSector = latMaxGlobal-(Double.valueOf(i)*(diferenciaLat/10));
			//el minimo en latitud del sector, limite inferior del sector
			double latInicialSector = latMaxGlobal - (Double.valueOf(i+1)*(diferenciaLat/10));

			for (int j = 0; j < 9; j++) {

				double longMinSector = longMinGlobal + (Double.valueOf(i)*(diferenciaLong/10));
				double longMaxSector = longMinGlobal + (Double.valueOf(i+1)*(diferenciaLong/10));
				idCuenta++;
				sectoresChain.put(idCuenta, new VOSector(idCuenta, longMaxSector, longMinSector, latFinalSector, latInicialSector));
				sectoresProbing.put(idCuenta, new VOSector(idCuenta, longMaxSector, longMinSector, latFinalSector, latInicialSector));
			}
		}
	}

	public VOSector sectorDelPunto(double longitudP, double latitudP){
		VOSector sector1= null;
		VOSector sector2= null;
		for (int j = 0; j <= 100 && sector2==null; j++) {
  			sector1=sectoresChain.get(j);
			if(sector1!=null) {
				if(sector1.getMaxLatitud()>=latitudP && sector1.getMinLatitud()<=latitudP && sector1.getMinLongitud()<=longitudP && sector1.getMaxLongitud()>=longitudP ) {
					sector2=sector1;
				}
			}
		}
		return sector2;
	}
	
	public VOSector sectorDelPuntoLinearPro(double longitudP, double latitudP){
		VOSector sector1= null;
		VOSector sector2= null;
		for (int j = 0; j <= 100; j++) {
			sector1=sectoresProbing.get(j);
			if(sector1!=null) {
				if(sector1.getMaxLatitud()>=latitudP && sector1.getMinLatitud()<=latitudP && sector1.getMinLongitud()<=longitudP && sector1.getMaxLongitud()>=longitudP && sector2==null) {
					sector2=sector1;
				}
			}
		}
		return sector2;
	}
	
	public DoublyLinkedList<VOBikeRoute> bicicletasEnUnSectorLinearPro(double longitudP, double latitudP){
		DoublyLinkedList<VOBikeRoute> bicicletasDelSector=new DoublyLinkedList<VOBikeRoute>();
		VOSector sector3=sectorDelPuntoLinearPro(longitudP, latitudP);
		bicicletasDelSector=sector3.getCicloRutasPorSector();
		return bicicletasDelSector;
	}



	public DoublyLinkedList<VOBikeRoute> bicicletasEnUnSector(double longitudP, double latitudP){
		DoublyLinkedList<VOBikeRoute> bicicletasDelSector=new DoublyLinkedList<VOBikeRoute>();
		VOSector sector3=sectorDelPunto(longitudP, latitudP);
		bicicletasDelSector=sector3.getCicloRutasPorSector();
		return bicicletasDelSector;
	}

	public void cargarBicisASectoresChainHash(){
		DoublyLinkedList<VOBikeRoute> bicicletasDelSector=new DoublyLinkedList<VOBikeRoute>();
		VOSector sector3=null;

		for (int i = 0; i < 100; i++) {

			sector3= sectoresChain.get(i+1);
			if(sector3!=null) {

				double maxLat=sector3.getMaxLatitud();
				double maxLong=sector3.getMaxLatitud();
				double minLat=sector3.getMinLatitud();
				double minLong=sector3.getMinLongitud();

				for (VOBikeRoute voBikeRoute : routes) {

					GeoCoordinate[] temp = voBikeRoute.getRoute();
					boolean primerPuntoMaximos= maxLat >= temp[0].getLatitud() && maxLong>=temp[0].getLongitud();
					boolean primerPuntoMinimos= minLat <= temp[0].getLatitud() && minLong<=temp[0].getLongitud();
					boolean segundoPuntoMaximos= maxLat >= temp[temp.length-1].getLatitud() && maxLong>=temp[temp.length-1].getLongitud();
					boolean segundoPuntoMinimos= minLat <= temp[temp.length-1].getLatitud() && minLong<=temp[temp.length-1].getLongitud();
					if((primerPuntoMaximos && primerPuntoMinimos) || (segundoPuntoMaximos && segundoPuntoMinimos)) {
						bicicletasDelSector.add(voBikeRoute);
					}

				}
				sector3.setCicloRutasPorSector(bicicletasDelSector);

			}
		}

	}


	public void cargarBicisASectoresLinearPro(){
		DoublyLinkedList<VOBikeRoute> bicicletasDelSector=new DoublyLinkedList<VOBikeRoute>();
		VOSector sector3=null;

		for (int i = 0; i < 100; i++) {

			sector3= sectoresProbing.get(i+1);
			if(sector3!=null) {

				double maxLat=sector3.getMaxLatitud();
				double maxLong=sector3.getMaxLatitud();
				double minLat=sector3.getMinLatitud();
				double minLong=sector3.getMinLongitud();

				for (VOBikeRoute voBikeRoute : routes) {

					GeoCoordinate[] temp = voBikeRoute.getRoute();
					boolean primerPuntoMaximos= maxLat >= temp[0].getLatitud() && maxLong>=temp[0].getLongitud();
					boolean primerPuntoMinimos= minLat <= temp[0].getLatitud() && minLong<=temp[0].getLongitud();
					boolean segundoPuntoMaximos= maxLat >= temp[temp.length-1].getLatitud() && maxLong>=temp[temp.length-1].getLongitud();
					boolean segundoPuntoMinimos= minLat <= temp[temp.length-1].getLatitud() && minLong<=temp[temp.length-1].getLongitud();
					if((primerPuntoMaximos && primerPuntoMinimos) || (segundoPuntoMaximos && segundoPuntoMinimos)) {
						bicicletasDelSector.add(voBikeRoute);
					}

				}
				sector3.setCicloRutasPorSector(bicicletasDelSector);

			}
		}

	}

	
	public DoublyLinkedList<VOBikeRoute> getRoutes(){
		return routes;
	}


	//--------------------------------------------------------
	//Metodos auxiliares
	//--------------------------------------------------------



	private double darMayorLongitudRuta(VOBikeRoute pRuta) {
		double mayor=-1000000000000.0;
		for (int i = 0; i < pRuta.getRoute().length; i++) {
			GeoCoordinate temp = pRuta.getRoute()[i];
			if(temp.getLongitud() > mayor) {
				mayor = temp.getLongitud();
			}
		}
		return mayor;
	}

	private double darMenorLongitudRuta(VOBikeRoute pRuta) {
		double menor=Double.MAX_VALUE;
		for (int i = 0; i < pRuta.getRoute().length; i++) {
			GeoCoordinate temp = pRuta.getRoute()[i];
			if(temp.getLongitud() < menor) {
				menor = temp.getLongitud();
			}

		}
		return menor;
	}

	private double darMayorLatitudRuta(VOBikeRoute pRuta) {
		double mayor=-1000000000.01;
		for (int i = 0; i < pRuta.getRoute().length; i++) {
			GeoCoordinate temp = pRuta.getRoute()[i];
			if(temp.getLatitud() > mayor) {
				mayor = temp.getLatitud();
			}

		}
		return mayor;
	}

	private double darMenorLatitudRuta(VOBikeRoute pRuta) {
		double menor=Double.MAX_VALUE;
		for (int i = 0; i < pRuta.getRoute().length; i++) {
			GeoCoordinate temp = pRuta.getRoute()[i];
			if(temp.getLatitud() < menor) {
				menor = temp.getLatitud();
			}
		}
		return menor;
	}

	private double mayorLongitudRutas() {
		double max=-100000000.0;
		for (VOBikeRoute voBikeRoute : routes) {
			if(darMayorLongitudRuta(voBikeRoute)>max) {
				max=darMayorLongitudRuta(voBikeRoute);
			}
		}
		return max;
	}

	private double menorLongitudRutas() {
		double min=Double.MAX_VALUE;
		for (VOBikeRoute voBikeRoute : routes) {
			if(darMenorLongitudRuta(voBikeRoute)<min) {
				min=darMenorLongitudRuta(voBikeRoute);
			}
		}
		return min;
	}

	private double mayorLatitudRutas() {
		double max=0;
		for (VOBikeRoute voBikeRoute : routes) {
			if(darMayorLatitudRuta(voBikeRoute)>max) {
				max=darMayorLatitudRuta(voBikeRoute);
			}
		}
		return max;
	}
	private double menorLatitudRutas() {
		double min=Double.MAX_VALUE;
		for (VOBikeRoute voBikeRoute : routes) {
			if(darMenorLatitudRuta(voBikeRoute)<min) {
				min=darMenorLatitudRuta(voBikeRoute);
			}
		}
		return min;
	}


}