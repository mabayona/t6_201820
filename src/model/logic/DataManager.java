package model.logic;

import java.io.FileReader;
import java.io.IOException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import model.data_structures.DoublyLinkedList;
import model.vo.GeoCoordinate;
import model.vo.VOBikeRoute;

public class DataManager  {

	public DoublyLinkedList<VOBikeRoute> loadBikeRoutesJSON(String jsonRoute) {
		DoublyLinkedList<VOBikeRoute> routes = new DoublyLinkedList<VOBikeRoute>();
		FileReader file;
		try {
			file = new FileReader(jsonRoute);
			JSONParser parser = new JSONParser();
			JSONArray js = (JSONArray) parser.parse(file);

			System.out.println(js.size());
			//JSONArray rutasJs = (JSONArray) js.get("data"); // rutas: la lista de rutas que se encuentran en data
			routes = loadArrayOfRoutesJSON(js); // Actualiza routes con todas las rutas del JSON

			file.close();
		} catch (IOException | ParseException e) {
		
			e.printStackTrace();
		}

		return routes;
	}

	/**
	 * Metodo auxiliar de loadBikeRoutesJSON()
	 * Carga todas las rutas del JSONArray que se pasa por parametro en una lista encadenada
	 * @Return retorna una lista encadenada con las rutas cargadas
	 */
	private DoublyLinkedList<VOBikeRoute> loadArrayOfRoutesJSON(JSONArray rutas) {
		DoublyLinkedList<VOBikeRoute> VOBikeRoutestArray = new DoublyLinkedList<VOBikeRoute>();
		if(rutas != null) {
			for(int i = 0; i < rutas.size(); i++) {
				JSONObject bikeRoute = (JSONObject) rutas.get(i);
				VOBikeRoute loadedRoute = loadRouteDataJSON(bikeRoute);
				VOBikeRoutestArray.add(loadedRoute);
			}
		}
		return VOBikeRoutestArray;
	}
	/**
	 * Metodo auxiliar de loadArrayOfRoutesJSON(JSONArray rutas)
	 * Lee los datos de una ruta del JSON Array que se pasa por parametro 
	 * @Return Un objeto de tipo VOBikeRoute con todos los datos necesarios de la ruta, o NULL si no pudo leer rutaData
	 */
	private VOBikeRoute loadRouteDataJSON(JSONObject rutaData) {
		VOBikeRoute VO = null;
		
			String id = (String) rutaData.get("id");
			String type = (String) rutaData.get("BIKEROUTE");
			String location = (String) rutaData.get("the_geom");
			String referenceStreet = (String) rutaData.get("STREET");
			String endStreet1 = (String) rutaData.get("F_STREET");
			String endStreet2 = (String) rutaData.get("T_STREET");
			String lenght = (String) rutaData.get("Shape_Leng");
			GeoCoordinate[] coordenadas = this.darArregloCoordRuta(location);

			VO = new VOBikeRoute(id, type, coordenadas, referenceStreet, endStreet1, endStreet2, Double.valueOf(lenght));

			/** ORDEN DE PARAMETROS
			 * 		this.id = id;
			 *  	this.bikeRouteType = bikeRouteType;
					this.Locationcoordinates = locationcoordinates;
					this.referenceStreet = referenceStreet;
					this.endStreet1 = endStreet1;
					this.endStreet2 = endStreet2;
					this.length = length;
			 */
		return VO;
	}
	
	private GeoCoordinate[] darArregloCoordRuta(String rutaP) {
		
		//System.out.println(rutaP);
		
		GeoCoordinate[] coordinatesArray = null;
		
		// "LINESTRING	(-87.6167128297411	41.803918566580315,	-87.61670796371423	41.80370020088833,	87.61669998425425	41.803396030230424,	-87.61669042794503	41.80301599776258,	-87.61668988905507	41.80299524758898,	-87.61664548329277	41.80252048501486,	-87.61663458295139	41.8020965357264)"
		
		String aModificar= rutaP.replace("LINESTRING", "");
		//System.out.println(aModificar);

		String aModificar2= aModificar.replace("(", "");
		//System.out.println(aModificar2);

		String aModificar3= aModificar2.replace(")", "");
		//System.out.println(aModificar3);

		String[] arr = aModificar3.split(",");
		
		coordinatesArray = new GeoCoordinate[arr.length];
		for(int i = 0; i < arr.length; i++) {
			String temp = arr[i];
			String[] arr2 = temp.split(" "); //-87.6167128297411	41.803918566580315 // longitud latitud
			//System.out.println(arr2[2] + "---" +arr2[1]);

			GeoCoordinate nuevaCoo = new GeoCoordinate(Double.valueOf(arr2[1]), Double.valueOf(arr2[2]));
			coordinatesArray[i] = nuevaCoo;
		}
		return coordinatesArray;
	}




}
