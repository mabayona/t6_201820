package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.DoublyLinkedList;
import model.vo.VOBikeRoute;
import model.vo.VOSector;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				Controller.loadBikeRoutesJSON();

				for(VOBikeRoute temp : Controller.getBikeRoutes())
					System.out.println(temp.toString());
				System.out.println("-----------------------------------------------------------");
				System.out.println("Cantidad de ciclorutas que se cargaron al sistema: " + Controller.getBikeRoutes().getSize());
				System.out.println("-----------------------------------------------------------");

				break;

			case 2:

				System.out.println(Controller.latitudesLongitudesMaxMin());

				break;

			case 3:
				System.out.println("Ingrese la longitud (double):");
				System.out.println("Coordenadas dentro de 1 sector: -87.78396451565798, -87.81235055856227");

				double longitud = Double.parseDouble(sc.next());

				System.out.println("Ingrese la latitud (double):");
				System.out.println("Coordenadas dentro de 1 sector: 42.020882125851536, 41.98459120239463");
				double latitud = Double.parseDouble(sc.next());

				VOSector sector1= Controller.SectorDelPunto(longitud, latitud);
				int numeroSector = sector1.getId();

				DoublyLinkedList<VOBikeRoute> lista=Controller.bicicletasEnUnSector(longitud, latitud);

				if(lista!=null) {

					System.out.println("N�mero del sector de consulta de la localizaci�n de consulta: "+numeroSector);
					for (VOBikeRoute voBikeRoute : lista) {
						System.out.println("Calle de referencia: "+voBikeRoute.getReferenceStreet());
						boolean primerPuntoContenidoLongitud=voBikeRoute.getFirstCoord().getLongitud()< sector1.getMaxLongitud() && voBikeRoute.getFirstCoord().getLongitud()> sector1.getMinLongitud();
						boolean primerPuntoContenidoLatitud=voBikeRoute.getFirstCoord().getLatitud()< sector1.getMaxLatitud() && voBikeRoute.getFirstCoord().getLatitud()> sector1.getMinLatitud();
						boolean segundoPuntoContenidoLatitud=voBikeRoute.getLastCoord().getLatitud()< sector1.getMaxLatitud() && voBikeRoute.getLastCoord().getLatitud()> sector1.getMinLatitud();
						boolean segundoPuntoContenidoLongitud=voBikeRoute.getLastCoord().getLongitud()< sector1.getMaxLongitud() && voBikeRoute.getLastCoord().getLongitud()> sector1.getMinLongitud();
						System.out.println("Punto/s iniciales de la cicloruta contenidos en el sector (long, lat): ");

						if( primerPuntoContenidoLatitud && primerPuntoContenidoLongitud) {
							System.out.println(voBikeRoute.getFirstCoord().getLongitud()+","+voBikeRoute.getFirstCoord().getLatitud());
						}
						if(segundoPuntoContenidoLatitud && segundoPuntoContenidoLongitud) {
							System.out.println(voBikeRoute.getLastCoord().getLongitud()+","+voBikeRoute.getLastCoord().getLatitud());
						}
					}

				}else {
					System.out.println("No hay ciclorutas que comienzan en el sector de consulta o la localizaci�n de consulta No est� " + 
							"al	interior de	un sector");
				}
				break;

			case 4:
				System.out.println("Ingrese la longitud (double):");
				System.out.println("Coordenadas dentro de 1 sector: -87.78396451565798, -87.81235055856227");

				double longitud2 = Double.parseDouble(sc.next());

				System.out.println("Ingrese la latitud (double):");
				System.out.println("Coordenadas dentro de 1 sector: 42.020882125851536, 41.98459120239463");

				double latitud2 = Double.parseDouble(sc.next());

				VOSector sector12= Controller.SectorDelPuntoLinearPro(longitud2, latitud2);
				int numeroSector2 = sector12.getId();

				DoublyLinkedList<VOBikeRoute> lista2=Controller.bicicletasEnUnSector(longitud2, latitud2);

				if(lista2!=null) {

					System.out.println("N�mero del sector de consulta de la localizaci�n de consulta: "+numeroSector2);
					for (VOBikeRoute voBikeRoute : lista2) {
						System.out.println("Calle de referencia: "+voBikeRoute.getReferenceStreet());
						boolean primerPuntoContenidoLongitud2=voBikeRoute.getFirstCoord().getLongitud()< sector12.getMaxLongitud() && voBikeRoute.getFirstCoord().getLongitud()> sector12.getMinLongitud();
						boolean primerPuntoContenidoLatitud2=voBikeRoute.getFirstCoord().getLatitud()< sector12.getMaxLatitud() && voBikeRoute.getFirstCoord().getLatitud()> sector12.getMinLatitud();
						boolean segundoPuntoContenidoLatitud2=voBikeRoute.getLastCoord().getLatitud()< sector12.getMaxLatitud() && voBikeRoute.getLastCoord().getLatitud()> sector12.getMinLatitud();
						boolean segundoPuntoContenidoLongitud2=voBikeRoute.getLastCoord().getLongitud()< sector12.getMaxLongitud() && voBikeRoute.getLastCoord().getLongitud()> sector12.getMinLongitud();
						System.out.println("Punto/s iniciales de la cicloruta contenidos en el sector (long, lat): ");

						if( primerPuntoContenidoLatitud2 && primerPuntoContenidoLongitud2) {
							System.out.println(voBikeRoute.getFirstCoord().getLongitud()+","+voBikeRoute.getFirstCoord().getLatitud());
						}
						if(segundoPuntoContenidoLatitud2 && segundoPuntoContenidoLongitud2) {
							System.out.println(voBikeRoute.getLastCoord().getLongitud()+","+voBikeRoute.getLastCoord().getLatitud());
						}
					}
				}
				else {
					System.out.println("No hay ciclorutas que comienzan en el sector de consulta o la localizaci�n de consulta No est� " + 
							"al	interior de	un sector");
				}
				break;

			case 5:	
				fin = true;
				sc.close();
				break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 6----------------------");
		System.out.println("1. Cree una nueva coleccion de ciclo rutas");
		System.out.println("2. Latitudes y longitudes maximas y minimas");
		System.out.println("3. Lista de bicicletas en un sector dadas una latitud y una longitud (Separate Chaining)");
		System.out.println("4. Lista de bicicletas en un sector dadas una latitud y una longitud (Linear Probing)");
		System.out.println("5. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}
}
